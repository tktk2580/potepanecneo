require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  describe "title test" do
    include ApplicationHelper
    subject { full_title(page_title) }

    context "title is empty" do
      let(:page_title) { "" }

      it "input empty" do
        expect(full_title(page_title)).to eq "BIGBAG Store"
      end
    end

    context "title is nil" do
      let(:page_title) { nil }

      it "input nil" do
        expect(full_title(page_title)).to eq "BIGBAG Store"
      end
    end

    context "title is item name" do
      let(:page_title) { "Ruby on Rails Baseball" }

      it "input item name" do
        expect(full_title(page_title)).to eq "Ruby on Rails Baseball - BIGBAG Store"
      end
    end
  end
end
