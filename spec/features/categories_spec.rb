require 'rails_helper'

RSpec.feature "Categories", type: :feature do
  let!(:taxonomy)      { create(:taxonomy, name: "Categories") }
  let!(:taxon)         { create(:taxon, name: "Bags", taxonomy: taxonomy, parent_id: taxonomy.root.id) }
  let!(:other_taxon)   { create(:taxon, name: "Mugs", taxonomy: taxonomy, parent_id: taxonomy.root.id) }
  let!(:productB1)     { create(:product, name: "Bag1", price: "110", taxons: [taxon]) }
  let!(:productB2)     { create(:product, name: "Bag2", price: "120", taxons: [taxon]) }
  let!(:productM1)     { create(:product, name: "Mug1", price: "210", taxons: [other_taxon]) }

  before do
    visit potepan_category_path(taxon.id)
  end

  scenario "page is displayed correctly" do
    expect(page).to have_content "Bag1"
    expect(page).to have_content "Bag2"
    expect(page).not_to have_content "Mug1"
    expect(page).to have_content "110"
    expect(page).to have_content "120"
    expect(page).not_to have_content "210"
  end

  scenario "indicate taxon's name and price when click taxonomy" do
    click_link "Categories"
    expect(page).to have_content "Bags"
    expect(page).to have_content "(2)"
  end

  scenario "indicate content when click taxon, don't indicate other taxon" do
    click_link "Categories"
    click_link "Mugs"
    expect(page).to have_content "Mug1"
    expect(page).not_to have_content "Bag1"
    expect(page).not_to have_content "Bag2"
  end

  scenario "click_link is right when click taxon" do
    click_link "Bags"
    expect(current_path).to eq potepan_category_path(taxon.id)
    expect(current_path).not_to eq potepan_category_path(other_taxon.id)
    click_link "Mugs"
    expect(current_path).not_to eq potepan_category_path(taxon.id)
    expect(current_path).to eq potepan_category_path(other_taxon.id)
  end

  scenario "click_link is right when click product" do
    click_link "Bag1"
    expect(current_path).to eq potepan_product_path(productB1.id)
  end
end
