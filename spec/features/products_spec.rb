require 'rails_helper'

RSpec.feature "Categories", type: :feature do
  let(:main_product) { create :product, name: "Bag0", price: "10", taxons: [taxon01, taxon1] }
  let!(:rel_product1) { create :product, name: "Bag1", price: "11", taxons: [taxon01, taxon1] }
  let!(:rel_product2) { create :product, name: "Mug0", price: "20", taxons: [taxon02, taxon1] }
  let!(:rel_product3) { create :product, name: "Mug1", price: "21", taxons: [taxon02, taxon1] }
  let!(:rel_product4) { create :product, name: "Shirts0", price: "30", taxons: [taxon03, taxon1] }
  let!(:rel_product5) { create :product, name: "Shirts1", price: "31", taxons: [taxon03, taxon1] }
  let!(:unrel_product00) { create :product, name: "Shirts2", price: "32", taxons: [taxon03, taxon2] }
  let(:taxon01) { create :taxon,  name: "Bags" }
  let(:taxon02) { create :taxon,  name: "Mugs" }
  let(:taxon03) { create :taxon,  name: "Shirts" }
  let(:taxon1) { create :taxon,  name: "Rails" }
  let(:taxon2) { create :taxon,  name: "Ruby" }

  scenario "see main product page and relative product" do
    visit potepan_product_path(main_product.id)
    expect(page).to have_content "Bag0"
    expect(page).to have_content "10"
    expect(page).to have_content "Bag1"
    expect(page).to have_content "Mug0"
    expect(page).to have_content "Mug1"
    expect(page).to have_content "Shirts0"
    expect(page).not_to have_content "Shirts1"
    expect(page).not_to have_content "Shirts2"
  end

  scenario "click relative product" do
    visit potepan_product_path(main_product.id)
    click_link "Bag1"
    expect(current_path).to eq potepan_product_path(rel_product1.id)
    expect(current_path).not_to eq potepan_product_path(main_product.id)
  end

  scenario "click back to list page" do
    visit potepan_product_path(main_product.id)
    click_link "一覧ページへ戻る"
    expect(current_path).to eq potepan_category_path(main_product.taxons.ids.first)
  end
end
