require 'rails_helper'

RSpec.describe "Potepan::Categories", type: :request do
  let(:taxonomy) { create(:taxonomy, name: "Categories") }
  let(:taxon)    { create(:taxon,    name: "Bags", taxonomy: taxonomy, parent_id: taxonomy.root.id) }
  let(:product)  { create(:product,  name: "Rails", taxons: [taxon]) }

  before do
    get potepan_category_path(taxon.id)
  end

  it "responds successfully" do
    expect(response).to be_successful
  end

  it "returns a 200 response" do
    expect(response).to have_http_status "200"
  end

  it "instant value is right" do
    expect(assigns(:taxonomies).first).to eq taxon.root
    expect(assigns(:taxon)).to eq taxon
    expect(assigns(:products)).to eq taxon.products
  end
end
