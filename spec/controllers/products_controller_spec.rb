require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :controller do
  describe "product page" do
    let(:product) { create :product, taxons: [taxon0, taxon2] }
    let(:rel_product) { create :product, taxons: [taxon0, taxon3] }
    let(:unrel_product) { create :product, taxons: [taxon1, taxon3] }
    let(:taxon0) { create :taxon,  name: "Bags" }
    let(:taxon1) { create :taxon,  name: "Mugs" }
    let(:taxon2) { create :taxon,  name: "Rails" }
    let(:taxon3) { create :taxon,  name: "Ruby" }

    before do
      get :show, params: { id: product.id }
    end

    it "is valid" do
      expect(product).to be_valid
    end

    it "responds successfully" do
      expect(response).to be_successful
    end

    it "returns a 200 response" do
      expect(response).to have_http_status "200"
    end

    it "instant value is right" do
      expect(assigns(:product)).to eq product
      expect(assigns(:related_products)).to contain_exactly(rel_product)
      expect(assigns(:related_products)).not_to contain_exactly(unrel_product)
    end
  end
end
