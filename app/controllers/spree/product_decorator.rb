module ProductDecorator
  def related_products(count)
    Spree::Product.
      in_taxons(taxons).
      where.not(id: id).
      distinct.
      includes(master: [:default_price, :images]).
      limit(count)
  end
  Spree::Product.prepend self
end
